require 'yaml'

file_path = "#{ENV['HOME']}/projects/learn_to_code/recipes/my_recipes.txt"

recipes = []
File.open(file_path, 'r') do |file|
  recipes = YAML.load file.read
end

def get_ingredients
  puts "now enter ingredients with quantity, when finish enter 'n'"
  ingredient_list = Hash.new
  continue = true
  while continue
    puts "enter ingredient:"
    ingredient = STDIN.gets.chomp
    puts "enter quantity"
    quantity = STDIN.gets.chomp
    ingredient_list[ingredient] = quantity
    puts "do you want to add another ingredient? y/n"
    choice = STDIN.gets.chomp
    if choice == 'n'
      continue = false
    end
  end
  return ingredient_list
end

def get_title
  puts "Add a recipe"
  puts "enter recipe title:"
  title = STDIN.gets.chomp
end

def get_time
  puts "enter preparing time:"
  time = STDIN.gets.chomp
end

def get_difficulty
  puts "enter difficulty (scale 1-10)"
  difficulty = STDIN.gets.chomp
end

def get_instructions
  puts "enter instructions"
  instructions = STDIN.gets.chomp
end

def display_recipe(recipe)
  puts 
  puts "#{recipe['title']} *,,*"
  puts "<><><><><><><><>"
  puts "ingredients:"
  recipe['ingredients'].each do |key, value|
    puts "#{key}: #{value}"
  end
  puts "<><><><><><><><>"
  puts "preparation time: #{recipe['time']}, difficulty: #{recipe['difficulty']}"
  puts "Instruction:"
  puts "#{recipe['instruction']}"
  puts "***************"
  puts "'b'- go back, 'e' - edit recipe, 'd' - delete recipe"
end

def manage_recipe(number, recipes)
  n = number - 1
  continue = true
  while continue
    choice = STDIN.gets.chomp
    if choice == 'e'
      puts "enter new title or empty for keeping the old one:"
      new_title = STDIN.gets.chomp
      recipes[n]['title'] = new_title unless new_title.empty?
      puts "enter new time or empty for keeping the old one:"
      new_time = STDIN.gets.chomp
      recipes[n]['time'] = new_time unless new_time.empty?
      puts "enter new difficulty or empty for keeping the old one:"
      new_difficulty = STDIN.gets.chomp
      recipes[n]['difficulty'] = new_difficulty unless new_difficulty.empty?
      puts "enter new instruction or empty for keeping the old one:"
      new_instruction = STDIN.gets.chomp
      recipes[n]['instruction'] = new_instruction unless new_instruction.empty?
      display_recipe(recipes[n])
    elsif choice == 'd'
      puts "are you sure you want to delete this recipe?(y/n)"
      answer = STDIN.gets.chomp
      recipes.delete_at(number-1) if answer == 'y'
      continue = false
    else
      continue = false
    end
  end
end

def filter_menu(recipes)
  filtered = []
  for i in 0..recipes.length
    filtered << i
  end
  return filtered
end

def display_menu(recipes, filter_1, filter_2)
  filtered = filter_1.call & filter_2.call
  puts
  puts "My recipes:"
  recipes.each_with_index do |recipe, index|
    puts "#{index+1}. #{recipe['title']} (#{recipe['difficulty']})" if filtered.include?(index)
  end
  puts "enter recipe number for details"
  puts "enter 'a' to add a recipe"
  puts "enter 'd' to search by difficulty"
  puts "enter 'i' to search by ingrednient"
  puts "enter 'r' to remove filter"
  puts "enter 'q' to quit"
end

def ask_for_difficulty
  puts "enter difficulty you want to search"
  max_difficulty = STDIN.gets.chomp.to_i
end

def ask_for_ingredient
  puts "enter ingredient you want to search"
  choice = STDIN.gets.chomp
end

def add_recipe(recipes)
  title = get_title
  time = get_time
  difficulty = get_difficulty
  ingredients = get_ingredients
  instructions = get_instructions
  new_entry = Hash.new
  new_entry["ingredients"] = ingredients
  new_entry["instruction"] = instructions
  new_entry["time"] = time
  new_entry["difficulty"] = difficulty
  new_entry["title"] = title
  recipes << new_entry
end

def all_indices(array)
  indices = []
  for i in 0..array.length
    indices << i
  end
  return indices
end

max_difficulty = nil
search_ingredient = nil

difficulty_filter = -> do
  unless max_difficulty
    return all_indices(recipes)
  end
  recipes.each_index.select do |i|
    recipes[i]['difficulty'].to_i <= max_difficulty.to_i
  end
end

ingredient_filter = -> do
  unless search_ingredient
    return all_indices(recipes)
  end
  recipes.each_index.select do |i|
    recipes[i]['ingredients'].keys.include? search_ingredient
  end
end

continue = true
while continue
  display_menu(recipes, difficulty_filter, ingredient_filter)
  choice = STDIN.gets.chomp
  if choice == 'a'
    add_recipe(recipes)
  elsif choice == 'q'
    text = recipes.to_yaml
    File.open(file_path, 'w') do |file|
      file.write text
    end
    continue = false
  elsif choice == 'd'
    max_difficulty = ask_for_difficulty
  elsif choice == 'i'
    search_ingredient = ask_for_ingredient
  elsif choice == 'r'
    max_difficulty = nil
    search_ingredient = nil
  else
    display_recipe(recipes[choice.to_i-1])
    manage_recipe(choice.to_i, recipes)
  end
end
